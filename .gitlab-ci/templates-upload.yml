######
# stage: upload

.principal-branches-only: &principal-branches-only |
    if [ "$UPLOAD_ARTIFACTS" != 1 ]
    then
      echo "⏹️ Not uploading artifacts, set UPLOAD_ARTIFACTS=1 to force"
      exit 0
    fi

.upload-snippet: &upload-snippet
    - *principal-branches-only
    # see https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - eval $(ssh-agent -s)
    - chmod 600 "$ARCHIVE_SECRET_FILE"
    - ssh-add "$ARCHIVE_SECRET_FILE"
    - echo Uploading ${source} to ${upload_dest}/${target}/
    - ssh -oStrictHostKeyChecking=no -p ${upload_port} ${upload_host} mkdir -p ${UPLOAD_ROOT}/${target}/
    - set -x; rsync -e "ssh -p $upload_port -oStrictHostKeyChecking=no" -aP --no-times --no-group --no-owner ${upload_exclude:+--exclude "$upload_exclude"} ${source} ${upload_dest}/${target}/; set +x

.upload-rules: &upload-rules
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$ARCHIVE_SECRET_FILE == null'
      when: never
    - if: $CI_COMMIT_BRANCH
      when: on_success

.try-immediate-upload-artifacts:
  variables: &artifacts-upload-variables
    target: daily/$release
    upload_dest: ${upload_host}:${UPLOAD_ROOT}
    source: a/$PIPELINE_VERSION
  after_script:
    - test -n "$ARCHIVE_SECRET_FILE" || exit
    - *principal-branches-only
    - |
      if [ "$CI_JOB_STATUS" != success ]
      then
        echo "⏹️ Job status is '$CI_JOB_STATUS', not uploading to ${upload_dest}/${target}}"
        exit 1
      fi
    - upload_exclude=${upload_exclude:-${repo:-}}
    - *upload-snippet
    - echo Delete ${source} since it has been already uploaded to ${upload_dest}/${target}/
    - find "${source}" -type f ! \( -name '*.licenses.gz' -o -name '*.pkglist.gz' \) -delete
    - find "${source}" -empty -type d -delete

.artifacts-upload:
  stage: artifacts upload
  interruptible: false # avoid partial uploads
  variables: *artifacts-upload-variables
  script:
    - *upload-snippet
  rules: *upload-rules

.sysroot-metadata-upload:
  stage: sysroot metadata upload
  interruptible: false # avoid partial uploads
  variables:
    target: sysroot/${release}
    upload_dest: ${upload_host}:${UPLOAD_ROOT}
    source: a/sysroot/${release}/*
  script:
    - *upload-snippet
  rules: *upload-rules

.installer-upload:
  stage: installer upload
  interruptible: false # avoid partial uploads
  variables: *artifacts-upload-variables
  script:
    - *upload-snippet
  rules: *upload-rules

.ostree-push:
  stage: ostree push
  interruptible: false # avoid partial uploads
  variables:
    repo: repo-${architecture}-${board}-${type}/
    branch: ${osname}/${release}/${architecture}-${board}/${type}
    upload_dest: ssh://${upload_host}:${upload_host_ostree_port}/${UPLOAD_ROOT}
    ssh: ssh -p ${upload_host_ostree_port} ${upload_host}
  script:
    - *principal-branches-only
    # see https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - eval $(ssh-agent -s)
    - chmod 600 "$ARCHIVE_SECRET_FILE"
    - ssh-add "$ARCHIVE_SECRET_FILE"
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - echo Uploading ${branch} from ${repo}
    - ${ssh} mkdir -p "${UPLOAD_ROOT}/${ostree_path}"
    - ${ssh} ostree init --repo="${UPLOAD_ROOT}/${ostree_path}" --mode=archive-z2 --collection-id=${collection_id}
    - echo Pushing to ${upload_dest}/${ostree_path}
    - ostree-push -o SetEnv=OSTREE_RECEIVE_CONF="${ostree_receive_conf}" --repo ${repo} ${upload_dest}/${ostree_path} ${branch}
    - ${ssh} ostree summary --update --repo="${UPLOAD_ROOT}/${ostree_path}" --verbose
  rules: *upload-rules

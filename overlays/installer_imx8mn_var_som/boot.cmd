env set file /deb-binaries/usr/lib/u-boot/imx8mn_var_som/flash.bin
env set file_addr_r ${kernel_addr_r}
env set file_offset 0x0
env set file_size 0x1000
env set mmc_devs 0 1 2
env set sd_part 1

env set cmd_exit_err '
  echo "+-----------------------------------------------------------------+";
  echo "|                   U-Boot installation FAILED                    |";
  echo "|                                                                 |";
  echo "| Please check the SD Card and power cycle the board to continue  |";
  echo "+-----------------------------------------------------------------+";
  loop 0 0'

env set cmd_exit_ok '
  echo "+-----------------------------------------------------------------+";
  echo "|                  U-Boot installation complete                   |";
  echo "|                                                                 |";
  echo "| Please remove the SD Card and power cycle the board to continue |";
  echo "+-----------------------------------------------------------------+";
  loop 0 0'

for d in ${mmc_devs}; do
  if test -z "${emmc_dev}" && mmc partconf ${d}; then
    env set emmc_dev ${d}
    echo "Found eMMC device: MMC ${emmc_dev}"
  fi
done

if test -z "${emmc_dev}"; then
    echo "Failed to find eMMC device"
    run cmd_exit_err
fi

for d in ${mmc_devs}; do
  if test -z "${sd_dev}" && test -e mmc ${d}:${sd_part} ${file}; then
    env set sd_dev ${d}
    echo "Found u-boot image in: MMC ${sd_dev}:${sd_part}"
  fi
done

if test -z "${sd_dev}"; then
    echo "Failed to find u-boot image"
    run cmd_exit_err
fi

echo "Copying u-boot image from MMC ${sd_dev}:${sd_part} to RAM ${file_addr_r}"
load mmc ${sd_dev}:${sd_part} ${file_addr_r} ${file}
if test $? -ne 0; then
  echo "Failed to copy u-boot image to RAM"
  run cmd_exit_err
fi

echo "Writing U-Boot to eMMC boot partition"
mmc dev ${emmc_dev} 1
mmc write ${file_addr_r} ${file_offset} ${file_size}
if test $? -ne 0; then
  echo "Failed to write U-Boot on eMMC boot partition"
  run cmd_exit_err
fi

echo "Configuring eMMC boot partition"
# Set eMMC boot bus configuration to SDR/DDR x8 dual data rate
mmc bootbus ${emmc_dev} 2 0 2
# Enable eMMC boot partition 1 and boot ACK
mmc partconf ${emmc_dev} 1 1 0
run cmd_exit_ok

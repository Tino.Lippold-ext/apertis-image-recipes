table.insert(alsa_monitor.rules,
  {
    matches = {
      {
        -- Matches all sources.
        { "node.name", "matches", "alsa_input.*" },
        { "api.alsa.card.name", "=", "Intel 82801AA-ICH" },
      },
      {
        -- Matches all sinks.
        { "node.name", "matches", "alsa_output.*" },
        { "api.alsa.card.name", "=", "Intel 82801AA-ICH" },
      },
    },
    apply_properties = {
      ["api.alsa.period-size"]   = 256,
      ["api.alsa.headroom"]      = 8192,
    }
  }
)

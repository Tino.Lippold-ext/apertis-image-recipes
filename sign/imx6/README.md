This directory contains the Apertis super root keys used for
signing U-Boot and FIT kernel image for i.MX-based boards (SabreLite).

The private keys come from the [HABv4 Apertis keys
repository](https://gitlab.apertis.org/infrastructure/apertis-imx-srk).

Our keys are not meant to be actually secret since they are only for
demonstrational purposes.

The goal is to show how product teams can use their own keys rathet than
providing any kind of protection for the test devices.

Given that once a HAB key is flashed on the i.MX6 boards it can't be changed,
it is desirable to use a widely available key for testing to avoid bricking
the device.

The private keys are also set as secrets in CI/CD to provide a more appropriate
solution for actual usage from product teams: real private keys should **not** be
committed to git.

The [CST tool](https://gitlab.apertis.org/pkg/development/imx-code-signing-tool) is needed
to sign binaries with the help of templates provided in this directory as well.

For correct boot you have to fuse the board with the signature from the file `SRK_1_2_3_4_table.bin`.

More information can be obtained from:
- https://gitlab.apertis.org/third-party/u-boot/blob/master/doc/imx/habv4/guides/mx6_mx7_secure_boot.txt
- https://boundarydevices.com/high-assurance-boot-hab-dummies/

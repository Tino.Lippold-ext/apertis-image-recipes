#! /bin/sh
set -eu

SUITE=${1:-v2024dev2}
MIRROR=${2:-https://repositories.apertis.org/apertis/}
OSNAME=${3:-apertis}

mkdir "${WORKSPACE}/chdist"

echo "Setup chdist"
chdist -d "${WORKSPACE}/chdist" create apertis-${SUITE} ${MIRROR} ${SUITE} target

echo "Add Apertis gpg key to chdist"
cp "${WORKSPACE}/keyring/${OSNAME}-archive-keyring.gpg" "${WORKSPACE}/chdist/apertis-${SUITE}/etc/apt/trusted.gpg.d"

echo "Update repo (chdist)"
chdist -d "${WORKSPACE}/chdist" -a arm64 apt apertis-${SUITE} update

echo "Download U-Boot (chdist)"
chdist -d "${WORKSPACE}/chdist" -a arm64 apt apertis-${SUITE} download arm-trusted-firmware

echo "Unpack U-Boot"
dpkg -x arm-trusted-firmware_*.deb "${WORKSPACE}/deb-binaries"

echo "Grab U-Boot"
mkdir -p "./arm-trusted-firmware/"
mv "${WORKSPACE}/deb-binaries/usr/lib/arm-trusted-firmware/rcar_uclb_h3/" "./arm-trusted-firmware/"

echo "Clean up"
rm -Rvf arm-trusted-firmware_*.deb "${WORKSPACE}/deb-binaries" "${WORKSPACE}/chdist"

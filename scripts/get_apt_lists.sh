#!/bin/bash

DISTRO=distro

usage(){
    echo "Usage: $0 [OPTIONS]
    Get the apt list for a given distro

    Options:
      -a: architecture
      -k: kerying file
      -m: mirror
      -s: suite
      -o: output folder
    " >&2
    exit 1
}

while getopts "a:k:m:o:s:" opt; do
    case $opt in
    h)
        usage
        ;;
    a)
        ARCH=$OPTARG
        ;;
    k)
        KEYRING=$OPTARG
        ;;
    m)
        MIRROR=$OPTARG
        ;;
    o)
        OUTPUT_FOLDER=$OPTARG
        ;;
    s)
        SUITE=$OPTARG
        ;;
    \?)
        echo Error: Unknown option >&2
        echo >&2
        usage
        exit 1
        ;;
    esac
    done
shift $((OPTIND -1))

if [ -z "$MIRROR" ] || [ -x "$SUITE" ] || [ -z "$ARCH" ] ; then
    echo "Please provide mirror, suite and architecture." >&2
    usage
    exit 1
fi

if [ -z "$OUTPUT_FOLDER" ] ; then
    echo "Please an output folder." >&2
    usage
    exit 1
fi

if [ -z "$KEYRING" ] ; then
    echo "Please provide a keyring file." >&2
    usage
    exit 1
fi

TMP_DIR=`mktemp -d`
chdist -d $TMP_DIR create $DISTRO $MIRROR $SUITE target development
cp $KEYRING $TMP_DIR/$DISTRO/etc/apt/trusted.gpg.d
chdist -d $TMP_DIR -a $ARCH apt $DISTRO update
mkdir -p $OUTPUT_FOLDER
cp $TMP_DIR/$DISTRO/var/lib/apt/lists/*Sources $OUTPUT_FOLDER
rm -r $TMP_DIR
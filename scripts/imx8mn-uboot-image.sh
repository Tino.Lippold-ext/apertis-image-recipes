#!/bin/sh

set -e

build_dir=$(mktemp -d)
firmware_dir=$ROOTDIR/deb-binaries/usr/lib/firmware/imx8mn/ddr/synopsys
uboot_dir=$ROOTDIR/deb-binaries/usr/lib/u-boot

usage() {
	echo "$0 [OPTIONS] <target> <flash.bin>
	Build <flash.bin> u-boot binary image for <target> board,
	including DDR firmware files (DDR4 by default).

	Packages u-boot and the proper imx-firmware package are
	expected to be installed by chdist.

	options:
	 -h           Display this help and exit
	 -d           Use DDR3 firmware files instead of DDR4.
	"
	exit 1
}

gen_mkimage_cfg() {
	# Generate mkimage configuration file.
	# Based on u-boot arch/arm/mach-imx/imx8m/imximage-8mn-ddr4.cfg
	cat << EOF
FIT
ROM_VERSION v2
BOOT_FROM sd
LOADER u-boot-spl-ddr.bin 0x912000
SECOND_LOADER u-boot.itb 0x40200000 0x60000
EOF
}

gen_boot_spl_ddr3() {
	# Generate u-boot-spl-ddr.bin file.
	# Based on u-boot tools/imx8m_image.sh
	objcopy -I binary -O binary --pad-to 0x8000 --gap-fill=0x0 ddr3_imem_1d.bin ddr3_imem_1d_pad.bin
	cat ddr3_imem_1d_pad.bin ddr3_dmem_1d.bin > ddr3_1d_fw.bin
	dd if=u-boot-spl.bin of=u-boot-spl-pad.bin bs=4 conv=sync
	cat u-boot-spl-pad.bin ddr3_1d_fw.bin > u-boot-spl-ddr.bin
	rm -f ddr3_1d_fw.bin ddr3_imem_1d_pad.bin u-boot-spl-pad.bin
}

gen_boot_spl_ddr4() {
	# Generate u-boot-spl-ddr.bin file.
	# Based on u-boot tools/imx8m_image.sh
	objcopy -I binary -O binary --pad-to 0x8000 --gap-fill=0x0 ddr4_imem_1d_201810.bin ddr4_imem_1d_pad.bin
	objcopy -I binary -O binary --pad-to 0x4000 --gap-fill=0x0 ddr4_dmem_1d_201810.bin ddr4_dmem_1d_pad.bin
	objcopy -I binary -O binary --pad-to 0x8000 --gap-fill=0x0 ddr4_imem_2d_201810.bin ddr4_imem_2d_pad.bin
	cat ddr4_imem_1d_pad.bin ddr4_dmem_1d_pad.bin > ddr4_1d_fw.bin
	cat ddr4_imem_2d_pad.bin ddr4_dmem_2d_201810.bin > ddr4_2d_fw.bin
	dd if=u-boot-spl.bin of=u-boot-spl-pad.bin bs=4 conv=sync
	cat u-boot-spl-pad.bin ddr4_1d_fw.bin ddr4_2d_fw.bin > u-boot-spl-ddr.bin
	rm -f ddr4_1d_fw.bin ddr4_2d_fw.bin ddr4_imem_1d_pad.bin ddr4_dmem_1d_pad.bin ddr4_imem_2d_pad.bin u-boot-spl-pad.bin
}

while getopts ":hd" opt; do
	case $opt in
	h)
		usage
		;;
        d)
                ddr_type=ddr3
                ;;
        \?)
                echo Error: Unknown option: -$OPTARG >&2
                echo >&2
                usage
                exit 1
                ;;
        esac
done
shift $((OPTIND -1))

if [ $# -ne 2 ]; then
	echo Error: Wrong arguments. >&2
	echo >&2
	usage
	exit 1
fi

target_board=$1
output_image=$2

if [ -z "${ddr_type}" ]; then
	ddr_type=ddr4
fi

mkdir -p $build_dir/

cp $firmware_dir/${ddr_type}_*.bin $build_dir/
cp $uboot_dir/$target_board/u-boot-spl.bin $build_dir/
cp $uboot_dir/$target_board/u-boot.itb $build_dir/

cd $build_dir

eval gen_boot_spl_${ddr_type}
gen_mkimage_cfg > imximage-8mn-ddr.cfg
mkimage -n imximage-8mn-ddr.cfg -T imx8mimage -e 0x912000 -d u-boot-spl-ddr.bin $output_image

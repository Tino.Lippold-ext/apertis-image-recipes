#!/bin/sh

if [ -z  "${RECIPEDIR}" ] ; then
  echo "RECIPEDIR not given"
  exit 1
fi

set -eu

UBOOT="$1"
UBOOT="$(realpath ${UBOOT})"

SIGNDIR=${RECIPEDIR}/sign/imx6
TEMPLATE=fit_image_csf.template
CSFCONF=u-boot-mx6qsabrelite.csf

# Need to change dir for CST
cd ${SIGNDIR}

# Values for u-boot.imx could be obtained from the build log
# See https://gitlab.apertis.org/third-party/u-boot/blob/master/doc/imx/habv4/guides/mx6_mx7_spl_secure_boot.txt

# Hardcoded values for SabreLite board
loadaddr="0x177ff400"
offset="0x00000000"
length="$(stat -c '%s' ${UBOOT})"
length="$(printf '0x%.8X' ${length})"

sed "${TEMPLATE}" \
    -e "s/{{loadaddr}}/${loadaddr}/" \
    -e "s/{{offset}}/${offset}/" \
    -e "s/{{length}}/${length}/" \
    -e "s#{{image}}#${UBOOT}#" > ${CSFCONF}

cst -i ${CSFCONF} -o u-boot.imx.bin

cat u-boot.imx.bin >> "${UBOOT}"


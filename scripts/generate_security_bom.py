#!/usr/bin/python3

import argparse
import dataclasses
import json
import os
import re
import requests
import sys
from dataclasses import dataclass
from debian import deb822
from json import JSONEncoder
from os.path import basename, isdir, isfile, join

GITLAB_SERVER = 'gitlab.apertis.org'

DEFAULT_METADATA_DIR = '/usr/share/doc'
DEFAULT_DPKG_STATUS = '/var/lib/dpkg/status'
DEFAULT_APT_LIST_DIR = '/var/lib/apt/lists'

BUILDINFO_DIR = 'buildinfo_dir'

ARCH_MAP = {
    'amd64': 'x86_64',
    'armhf': 'armv7hl',
    'arm64': 'aarch64'
}

def get_base_package_name(dirpath):
    return basename(dirpath)

def get_source_version(version):
    v = re.match(r'.+\+apertis\d+', version)
    if v:
        source_version = v[0]
    else:
        source_version = version

    return source_version

@dataclass(frozen=True)
class SourcePackage():
    name: str
    version: str

class CustomJSONEncoder(JSONEncoder):
        def default(self, o):
            if isinstance(o, set):
                return list(o)
            return dataclasses.asdict(o)

class BomGenerator():
    def __init__(self, gitlab_server, branch, arch, metadata_dir, packages_file, apt_list_dir):
        self.gitlab_server = gitlab_server
        self.branch = branch
        self.arch = arch
        self.metadata_dir = metadata_dir
        self.packages_file = packages_file
        self.apt_list_dir = apt_list_dir

        self.packages_to_sources = {}

    def get_packages_to_sources(self):
        for f in os.listdir(self.apt_list_dir):
            if not f.endswith('Sources'):
                continue

            sources = open(join(self.apt_list_dir, f)).read()
            source_packages = deb822.Sources(sources)

            for p in source_packages.iter_paragraphs(sources):
                source_package = p['Package']
                packages = [x.strip() for x in p['Binary'].split(',')]
                for p in packages:
                    self.packages_to_sources[p] = source_package

    def get_source_packages(self):
        packages_content = open(self.packages_file).read()
        packages = deb822.Packages(packages_content)

        source_packages = set()
        for p in packages.iter_paragraphs(packages_content):
            source_packages.add(SourcePackage(p.source, p['Version']))

        return source_packages

    def get_url_buildinfo(self, source_package, version):
        file_path = f'results/{source_package}_{version}_{self.arch}.buildinfo'
        url = (f'https://{self.gitlab_server}/api/v4/projects/pkg%2F{source_package}/jobs/artifacts/'
            f'{self.branch}/raw/{file_path}?job=obs-default-{ARCH_MAP[self.arch]}')

        return url

    # This implementation downloads Gitlab artifacts to extract buildinfo files.
    # This approach has limitations since only the last version of the artifacts
    # are allowed and only if the pipeline was run on the desired branch
    # A better approach will be possible when aptly stores buidinfo files.
    def get_buildinfo(self, source_packages, folder):
        try:
            os.mkdir(folder)
        except:
            pass
        for sp in source_packages:
            url = self.get_url_buildinfo(sp.name, sp.version)
            response = requests.get(url, stream=True)
            if response.status_code != 200:
                print(f'WARNING unable to download {url}', file=sys.stderr)
                continue
            buildinfo_file = folder + '/' + sp.name + '.buildinfo'
            with open(buildinfo_file, mode='wb') as file:
                for chunk in response.iter_content(chunk_size=10 * 1024):
                    file.write(chunk)

    def parse_build_deps(self, buildinfo):
        build_deps = set()
        for i in buildinfo['Installed-Build-Depends'].split('\n'):
            i = i.strip()
            if i == '':
                continue
            p = re.sub(r'[\(\)\=\,]', '', i)
            [package_name, version] = p.split('  ')
            source_package_name = self.packages_to_sources.get(package_name, package_name)
            build_deps.add(SourcePackage(source_package_name, get_source_version(version)))

        return build_deps

    def get_build_deps(self, buildinfo_dir):
        build_deps = {}
        for f in os.listdir(buildinfo_dir):
            buildinfo_content = open(join(buildinfo_dir, f)).read()
            buildinfo = deb822.BuildInfo(buildinfo_content)
            source_package = buildinfo['Source']
            version = buildinfo['Version']
            package_build_deps = self.parse_build_deps(buildinfo)
            build_deps[source_package] = {
                'name': source_package,
                'version': get_source_version(version),
                'build_deps': list(package_build_deps)
            }

        return build_deps

    def parse_metadata(self, f):
        metadata = {}
        shared_libraries = set()
        external_files = set()
        with open(f) as fm:
            metadata = json.load(fm)
            for p in metadata['referenced_source_packages']:
                pinfo = metadata['referenced_source_packages'][p]
                if 'shared_libraries' in pinfo['origins']:
                    shared_libraries.add(p)
                if 'external_files' in pinfo['origins']:
                    external_files.add(SourcePackage(p, pinfo['version']))

        referenced_packages = {'shared_libraries': shared_libraries, 'external_files': external_files}

        return referenced_packages

    def merge_metadata(self, metadata_a, metadata_b):
        result = {}
        for key in 'shared_libraries', 'external_files':
            result[key] = metadata_a.get(key, set()) | metadata_b.get(key, set())
        return result

    def scan_metadata(self):
        referenced_packages = {}
        for d in os.listdir(self.metadata_dir):
            dirpath = join(self.metadata_dir, d)
            if not isdir(dirpath):
                continue
            filenames = os.listdir(dirpath)
            if len(filenames) == 0:
                continue
            base_package_name = get_base_package_name(dirpath)
            for f in filenames:
                if not isfile(join(dirpath, f)) or f.find('_metadata_') == -1:
                    continue

                fparts = f.split('_')
                package_name = fparts[0]
                arch = fparts[2]

                source_package_name = self.packages_to_sources.get(package_name, package_name)

                f = join(dirpath, f)
                metadata = self.parse_metadata(f)
                if source_package_name not in referenced_packages:
                    referenced_packages[source_package_name] = metadata
                else:
                    referenced_packages[source_package_name] = (
                        self.merge_metadata(referenced_packages[source_package_name], metadata)
                    )

        return referenced_packages

    def combine_data(self, referenced_packages, build_deps):
        bom = build_deps.copy()
        for p in bom:
            if p in referenced_packages:
                bom[p].update(referenced_packages[p])
        for p in referenced_packages:
            if p not in bom:
                bom[p] = referenced_packages[p]

        return bom

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--arch", choices=['amd64', 'armhf', 'arm64'], help="architecture")
    parser.add_argument("-b", "--branch", help="branch")
    parser.add_argument("-d", "--dir", default=DEFAULT_METADATA_DIR, help="directory to search for information")
    parser.add_argument("-g", "--gitlab-server", default=GITLAB_SERVER, help="Gitlab server")
    parser.add_argument("-l", "--apt-list-dir", default=DEFAULT_APT_LIST_DIR, help="directory with apt lists")
    parser.add_argument("-o", "--out", help="output file")
    parser.add_argument("-s", "--dpkg-status", default=DEFAULT_DPKG_STATUS, help="dpkg status file")

    args = parser.parse_args()

    bom_generator = BomGenerator(args.gitlab_server, args.branch, args.arch, args.dir, args.dpkg_status, args.apt_list_dir)

    bom_generator.get_packages_to_sources()

    referenced_packages = bom_generator.scan_metadata()

    source_packages = bom_generator.get_source_packages()

    bom_generator.get_buildinfo(source_packages, BUILDINFO_DIR)

    build_deps = bom_generator.get_build_deps(BUILDINFO_DIR)

    bom = bom_generator.combine_data(referenced_packages, build_deps)

    if args.out:
        with open(args.out, 'w+') as  output:
            json.dump(bom, output, cls=CustomJSONEncoder)
    else:
        json.dumps(bom, cls=CustomJSONEncoder)

if __name__ == '__main__':
    main()
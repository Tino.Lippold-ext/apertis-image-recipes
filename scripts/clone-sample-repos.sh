#!/bin/sh

set -e

. /etc/os-release

REPOS="\
    helloworld-agentapp \
    helloworld-app \
    helloworld-https-client \
    helloworld-persistapp \
    helloworld-prefsapp \
    helloworld-simple-agent \
    cpp-library-example \
    hard-keys-example \
    notification-example \
"

echo "I: Cloning sample app repositories"
mkdir -p /home/user/sample-applications/

for r in $REPOS
do
    cd /home/user/sample-applications/
    git clone https://git.apertis.org/git/sample-applications/$r.git
    cd $r
    git rev-parse -q --verify "origin/${ID}/${VERSION_CODENAME}" && git checkout ${ID}/${VERSION_CODENAME}
done

chown user:user -R /home/user/sample-applications/

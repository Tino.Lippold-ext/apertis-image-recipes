#!/usr/bin/env python3

from pathlib import Path

import argparse
import gzip
import io
import shutil
import sys
import tarfile

from debian.deb822 import Packages
from debian.debian_support import Version


def map_packages_to_sources(root: Path) -> dict[str, tuple[str, Version]]:
    packages_to_sources: dict[str, tuple[str, Version]] = {}
    package_versions: dict[str, Version] = {}
    for sources in root.glob('var/lib/apt/lists/*Packages'):
        with sources.open() as fp:
            for package in Packages.iter_paragraphs(fp):
                name = package['package']
                version = package['version']
                # Only include the source for the newest version of the package.
                if name in package_versions and version <= package_versions[name]:
                    continue

                packages_to_sources[name] = (
                    package.source or name,
                    package.source_version,
                )
                package_versions[name] = version

    return packages_to_sources


def find_source_changelogs(root: Path) -> dict[str, Path]:
    packages_to_sources = map_packages_to_sources(root)

    source_changelogs: dict[str, Path] = {}
    source_versions: dict[str, Version] = {}

    for changelog in root.glob('usr/share/doc/*/changelog.Debian.gz'):
        package = changelog.parent.name
        if package not in packages_to_sources:
            # Some packages may have legacy /usr/share/doc symlinks under older
            # package names, in which case there is no assigned source package.
            # Just skip those, since we're going to also end up finding the
            # target of the link at some point anyway.
            continue

        source, version = packages_to_sources[package]
        if source in source_changelogs:
            previous_version = source_versions[source]
            if version != previous_version:
                print(
                    'WARNING: found two different versions of source package:'
                    + f' {version} (from {package}) vs {previous_version}',
                    file=sys.stderr,
                )

            # Only include the newest / most complete changelog we can find.
            if version <= previous_version:
                continue

        source_changelogs[source] = changelog
        source_versions[source] = version

    return source_changelogs


def main() -> None:
    parser = argparse.ArgumentParser(
        description='Save the changelogs from an image to a standalone tarball',
    )
    parser.add_argument(
        '--root',
        type=Path,
        help='path to the image root directory',
        required=True,
    )
    parser.add_argument(
        '--out',
        type=Path,
        help='output tarball path',
        required=True,
    )

    args = parser.parse_args()
    root: Path = args.root
    out: Path = args.out

    source_changelogs = find_source_changelogs(root)

    out_tmp = out.with_name(out.name + '.tmp')
    with tarfile.open(out_tmp, 'w:gz') as tar:
        for source, changelog in source_changelogs.items():
            # We need to read in the full changelog first in order to find out the
            # uncompressed size.
            contents = io.BytesIO()
            with gzip.open(str(changelog)) as fp:
                shutil.copyfileobj(fp, contents)

            info = tar.gettarinfo(name=str(changelog), arcname=f'{source}.changelog')
            info.size = contents.tell()

            contents.seek(0)
            tar.addfile(info, contents)

    out_tmp.rename(out)


if __name__ == '__main__':
    main()

#!/bin/sh

set -e

if [ "$#" -ne 4 ] ; then
    echo Usage create_repo.sh REPO_FOLDER DEB_FOLDER RELEASE ARCH
    exit 1
fi

REPO_FOLDER=$1
DEB_FOLDER=$2
RELEASE=$3
ARCH=$4

mkdir -p $REPO_FOLDER/conf
DIST=$REPO_FOLDER/conf/distributions

echo "Origin: apertis-test" > $DIST
echo "Label: apertis-test" >> $DIST
echo "Codename: $RELEASE" >> $DIST
echo "Architectures: $ARCH" >> $DIST
echo "Components: target" >> $DIST
echo "Description: Apertis test repo" >> $DIST

reprepro -b $REPO_FOLDER includedeb $RELEASE $DEB_FOLDER/*.deb

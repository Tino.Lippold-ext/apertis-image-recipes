#!/bin/sh

set -e

timestamp=$1
variant=$2

if [ -n "$variant" ]; then
    echo "VARIANT_ID=$variant" >> /etc/os-release
fi
if [ -n "$timestamp" ]; then
    echo "BUILD_ID=$timestamp" >> /etc/os-release
fi

#!/bin/sh

set -e

SUITE=$1

usage() {
    echo "Usage: $0 VERSION"
    echo "    VERSION   Apertis version number"
}

if [ -z "${SUITE}" ]; then
    usage
    exit 1
fi

# Configure Apertis flatpak repository
flatpak remote-add apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo
# Disable http2 since it currently does not support retries
ostree --repo=/var/lib/flatpak/repo config set 'remote "apertis".http2' false

# Install the Flatdeb example applications
flatpak install --noninteractive \
    org.apertis.hmi.gnome_font_viewer//${SUITE} \
    org.apertis.hmi.totem//stable

# Configure Flathub repository
flatpak remote-add --no-sign-verify flathub https://flathub.org/repo/flathub.flatpakrepo
# Disable http2 since it currently does not support retries
ostree --repo=/var/lib/flatpak/repo config set 'remote "flathub".http2' false

# Install Rhythmbox (music player)
flatpak install --noninteractive org.gnome.Rhythmbox3

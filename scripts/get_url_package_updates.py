#!/usr/bin/python3

import sys
import argparse
import gitlab
import requests

ARCH_MAP = {
    'amd64': 'x86_64',
    'armhf': 'armv7hl',
    'arm64': 'aarch64'
}

def connect(gitlab_instance, gitlab_server_url, gitlab_api_token):
    gl = None
    if gitlab_server_url:
        gl = gitlab.Gitlab(gitlab_server_url, private_token=gitlab_api_token)
    else:
        gl = gitlab.Gitlab.from_config(gitlab_instance)
    gl.auth()

    return gl

def get_latest_job_id(gl, project_path, mr_iid, arch):
    project = gl.projects.get(project_path)

    pmr = project.mergerequests.get(mr_iid, lazy = False)
    p = pmr.pipelines.list()[0]
    main_pipeline = project.pipelines.get(p.id)
    bp = main_pipeline.bridges.list()[0]
    downstream_pipeline = project.pipelines.get(bp.downstream_pipeline['id'])

    name = 'obs-default-' + ARCH_MAP[arch]

    for j in downstream_pipeline.jobs.list(iterator=True, lazy = True):
        if j.name == name:
            return j.id

    return None

def get_url(gitlab_server, project_path, job_id):
    project_path = requests.utils.quote(project_path, safe='')
    url = '{}/api/v4/projects/{}/jobs/{}/artifacts'.format(gitlab_server, project_path, str(job_id))

    return url

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--gitlab_instance', default='apertis', help='Gitlab instance to connect')
    parser.add_argument('--gitlab_server', default='gitlab.apertis.org', help='Gitlab server to connect')
    parser.add_argument('--gitlab_protocol', default='https', help='Gitlab protocol')
    parser.add_argument('--gitlab_token', help='Gitlab token')
    parser.add_argument('--mr_iid', help='Merge Request IID')
    parser.add_argument('--arch', choices = ARCH_MAP.keys(), help='Build architecture')
    parser.add_argument('project_path', help='Project path')

    args = parser.parse_args()

    gitlab_url = '{}://{}'.format(args.gitlab_protocol, args.gitlab_server)

    gl = connect(args.gitlab_instance, gitlab_url, args.gitlab_token)

    job_id = get_latest_job_id(gl, args.project_path, args.mr_iid, args.arch)

    if not job_id:
        print('Unable to find job', file=sys.stderr)
        sys.exit(1)

    print(get_url(gl.url, args.project_path, job_id))

if __name__ == '__main__':
    main(sys.argv[1:])

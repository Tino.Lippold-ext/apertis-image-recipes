#!/bin/sh

set -e

if [ "$#" -ne 3 ] ; then
    echo Usage download_package_updates.sh URL OUTPUT_FOLDER GITLAB_TOKEN
    exit 1
fi

PACKAGE_UPDATES_URL=$1
OUTPUT_FOLDER=$2
GITLAB_TOKEN=$3

echo Downloading package updates from $PACKAGE_UPDATES_URL to $OUTPUT_FOLDER

TMP_DIR=`mktemp -d`
OLD_DIR=`pwd`
cd $TMP_DIR
wget $PACKAGE_UPDATES_URL -O package_updates.zip --header="PRIVATE-TOKEN: $GITLAB_TOKEN"
unzip package_updates.zip
if ! [ -d $OUTPUT_FOLDER ] ; then
    echo Creating $OUTPUT_FOLDER
    mkdir $OUTPUT_FOLDER
fi
echo Moving packages to $OUTPUT_FOLDER
mv results/*deb $OUTPUT_FOLDER
cd $OLD_DIR
rm -r $TMP_DIR
echo Listing $OUTPUT_FOLDER
ls $OUTPUT_FOLDER

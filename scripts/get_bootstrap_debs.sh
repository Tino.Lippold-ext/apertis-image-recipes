#!/bin/sh

set -e

if [ "$#" -ne 4 ] ; then
    echo Usage get_bootstrap_debs.sh DEB_FOLDER MIRROR RELEASE ARCH
    exit 1
fi

DEB_FOLDER=$1
MIRROR=$2
RELEASE=$3
ARCH=$4

if ! [ -d $DEB_FOLDER ] ; then
    mkdir -p $DEB_FOLDER
fi

BOOTSTRAP_FOLDER=`mktemp -d`

debootstrap --no-check-gpg --arch $ARCH --components=target --variant=minbase --exclude=usr-is-merged --include apertis-archive-keyring --include ca-certificates --download-only $RELEASE $BOOTSTRAP_FOLDER $MIRROR sid

mv $BOOTSTRAP_FOLDER/var/cache/apt/archives/*deb $DEB_FOLDER
rm -r $BOOTSTRAP_FOLDER

#!/bin/sh

set -e

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade -y -q --no-install-recommends
if apt list --upgradable | grep -q upgradable ; then
   echo There are still packages to upgrade, abort!!!!
   exit 1
fi

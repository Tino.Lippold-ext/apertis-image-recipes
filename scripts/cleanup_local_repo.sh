#!/bin/sh

if [ "$#" -ne 2 ] ; then
    echo Usage cleanup_local_repo.sh ROOTDIR CHROOT_REPO_FOLDER
    exit 1
fi

ROOTDIR=$1
CHROOT_REPO_FOLDER=$2

SRCLIST=/etc/apt/sources.list

sed -i '/deb \[trusted=yes\] file/d' $ROOTDIR/$SRCLIST

umount $ROOTDIR/$CHROOT_REPO_FOLDER
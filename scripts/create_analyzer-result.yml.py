#!/usr/bin/env python3
# SPDX-License-Identifier: MIT
#
# Copyright Bosch.IO GmbH 2023. All rights reserved, also regarding any disposal, exploitation, reproduction,
# editing, distribution, as well as in the event of applications for industrial property rights.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial
# portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Script to prove the concept to use Apertis *.img.licenses.gz compliance data
and Debian metadata files to generate an analyzer-result.yml ORT file.
See README for details.
"""

import argparse
import json
import math as m
import os
import platform
import re
import requests
import subprocess
import sys
from datetime import *

class Package:
    """ Handle metadata of Apertis packages """

    def __init__(self, name, architecture, version, homepage, filename, sha256, description, plicense, pcopyright):
        self.name         = name
        self.architecture = architecture
        self.version      = version
        self.homepage     = homepage
        self.filename     = filename
        self.sha256       = sha256
        self.description  = description
        self.license      = plicense
        self.copyright    = pcopyright

    def name(self):
        return self.name

parser = argparse.ArgumentParser(
    description="Generate an analyzer-result.yml ORT file"
)
parser.add_argument(
    "--project",
    required=True,
    type=str,
    help="project name (e.g. Apertis-ORT-Demo:1.0.0)",
)
parser.add_argument(
    "--image",
    required=True,
    type=str,
    help="e.g. apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.gz",
)
parser.add_argument(
    "--release",
    required=True,
    type=str,
    help="e.g. v2024pre",
)
parser.add_argument(
    "--architecture",
    required=True,
    type=str,
    help="e.g. arm64",
)
parser.add_argument(
    "--release_date",
    required=True,
    type=str,
    help="e.g. 2023-06-21",
)
parser.add_argument(
    "--licenses",
    required=True,
    type=str,
    help="e.g. apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.licenses.gz",
)
parser.add_argument(
    "--pkglist",
    required=True,
    type=str,
    help="e.g. apertis_v2024dev2-hmi-amd64-uefi_v2024dev2.0.img.pkglist.gz",
)
parser.add_argument(
    "--repository",
    required=True,
    type=str,
    help="e.g. https://repositories.apertis.org/apertis/",
)
parser.add_argument(
    "--license_mapping",
    required=True,
    type=str,
    help="license mapping file in JSON format (e.g. license_mapping.json)",
)
parser.add_argument(
    "--repo_pkg",
    required=True,
    type=str,
    help="folder to temporarily store repositories Packages files",
)
parser.add_argument(
    "--result-file",
    required=True,
    type=str,
    help="output file in YAML format (e.g. analyzer-result.yml)",
)
args = parser.parse_args()

start_time = str(datetime.today()).replace(' ', 'T') + 'Z'

packages= []
list_installed_pkgs = []

## img.licenses.gz
subprocess.run(['gunzip', '-f', args.licenses])

## img.pkglist.gz
subprocess.run(['gunzip', '-f', args.pkglist])

## Packages files
pkg_repo_url = args.repository + "/dists/" + args.release
for pkg_repo in ["target", "non-free"]:
    pkg_repo_file = requests.get(pkg_repo_url+"/"+pkg_repo+"/"+"binary-"+args.architecture+"/Packages")
    if pkg_repo_file.status_code != 200:
        sys.exit(f'The ${pkg_repo} Packages file is not reachable!')
    with open(args.repo_pkg+"/"+pkg_repo+"_"+"Packages", "wb") as repopack:
        repopack.write(pkg_repo_file.content)

project = args.project
image = args.image
release_date= args.release_date
license_url = args.licenses
repository = args.repository

compliance_data = open(re.sub(".gz", "", args.licenses), 'r')
status = open(re.sub(".gz", "", args.pkglist), 'r')
for pkg_repo in ["target", "non-free"]:
    packages.append(open(args.repo_pkg+"/"+pkg_repo+"_"+"Packages", 'r'))
result = open(args.result_file, 'w')

license_mapping = open(args.license_mapping, 'r')

tail1 = '      source_artifact:\n        url: ""\n        hash:\n          value: ""\n          algorithm: ""\n'
tail2 = '      vcs:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""\n'
tail3 = '      vcs_processed:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""'
tail = tail1 + tail2 + tail3

status_list = status.readlines()

pdesc = []
pname = parch = pvers = phom = ''
separator = ' AND '

print('read installed packages from "status"!')
for line in status_list:
    if line.find('Package:') != -1:
        pname = line[line.find(': ') + 2:len(line)]
    elif line.find('Architecture:') != -1:
        parch = line[line.find(': ') + 2:len(line)]
    elif line.find('Version:') != -1:
        pvers = line[line.find(': ') + 2:len(line)]
    elif line.find('Description:') != -1:
        pdesc.append(line[line.find(': ') + 2:len(line)])
    elif line[0] == " " and not line[0:2] == ' /':
        pdesc.append(line[line.find(': ') + 2:len(line)])
    elif line.find('Homepage:') != -1:
        phom = line[line.find(': ') + 2:len(line)]
    elif line[0] == '\n':
        list_installed_pkgs.append(Package(pname, parch, pvers, phom, '', '', pdesc, '', ''))
        pname = parch = pvers = phom = ''
        pdesc = []

data = json.load(compliance_data)
list_pkgs = data["packages"]
license_mapping_data = json.load(license_mapping)

print('work on compliance data!')
for obj in list_installed_pkgs:
    for p in range(len(list_pkgs)):
        if obj.name.rstrip('\n') == list_pkgs[p]["package_name"]:
            pkg_licenses_list_mapped = []
            pkg_licenses_list = list_pkgs[p]["package_licenses"]
            for l in range(len(pkg_licenses_list)):
                pkg_licenses_list_mapped.append(str(license_mapping_data.get(pkg_licenses_list[l])))
            obj.license = pkg_licenses_list_mapped
            cl = str(pkg_licenses_list)
            if cl.find('NoSourceInfoFound') < 0 :
                obj.copyright = list_pkgs[p]["package_copyright"]

pname = pvers = pfilename = psha = ''

for pack in packages:
    print('search for installation paths in Packages file!')
    pack_list = pack.readlines()

    for line in pack_list:
        if line.find('Package') != -1:
            pname = line[line.find(': ') + 2:len(line)]
        elif line.find('Version') != -1:
            pvers = line[line.find(': ') + 2:len(line)]
        elif line.find('Filename') != -1:
            pfilename = line[line.find(': ') + 2:len(line)]
        elif line.find('SHA256') != -1:
            psha = line[line.find(': ') + 2:len(line)]
        elif line[0] == '\n':
            for obj in list_installed_pkgs:
                if obj.name == pname and obj.version == pvers:
                    obj.filename = pfilename
                    obj.sha256 = psha
                    pname = pvers = pfilename = psha = ''

end_time =  str(datetime.today()).replace(' ', 'T') + 'Z'

my_os = platform.system()
my_cpus = str(os.cpu_count())
my_mem = str(os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES'))
try:
    my_shell = os.environ['SHELL']
except KeyError:
    print("SHELL variable not defined!")
    my_shell = "NA"

try:
    my_term = os.environ['TERM']
except KeyError:
    print("TERM variable not defined!")
    my_term = "NA"

try:
    my_java_version = os.environ['JAVA_VERSION']
except KeyError:
    print("JAVA_VERSION variable not defined!")
    my_java_version = "NA"

try:
    my_ort_version = subprocess.check_output(['ort', '--version'], stderr=subprocess.STDOUT).decode("utf-8")
except OSError:
    print('ORT not detected!')
    my_ort_version = "NA"

result.write('---\nrepository:\n  vcs:\n    type: ""\n    url: ""\n    revision: ""\n    path: ""')
result.write('\n  vcs_processed:\n    type: ""\n    url: ""\n    revision: ""\n    path: ""\n  config: {}')
result.write('\nanalyzer:\n  start_time: "' + start_time + '"\n  end_time: "' + end_time +'"')
result.write('\n  environment:\n    ort_version: '+my_ort_version+'\n    java_version: '+my_java_version+'\n    os: '+my_os+'\n    processors: '+my_cpus+'\n    max_memory: '+my_mem)
result.write('\n    variables:\n      SHELL: '+my_shell+'\n      TERM: '+my_term+'\n    tool_versions: {}')
result.write('\n  config:\n    allow_dynamic_versions: false\n    skip_excluded: false')
result.write('\n    package_managers:')
result.write('\n      DotNet:\n        options:\n          directDependenciesOnly: "true"')
result.write('\n      NuGet:\n        options:\n          directDependenciesOnly: "true"')
result.write('\n  result:\n    projects:\n    - id: "Debian::' + project + '"')
result.write('\n      definition_file_path: "' + image +'"')
result.write('\n      declared_licenses: []\n      declared_licenses_processed: {}')
result.write('\n      vcs:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""')
result.write('\n      vcs_processed:\n        type: ""\n        url: ""\n        revision: ""\n        path: ""')
result.write('\n      homepage_url: "' + repository + image + '"\n      scope_names:\n      - "default"')
result.write('\n    packages:')

for obj in list_installed_pkgs:
    result.write('\n    - id: "Debian:' + obj.architecture.rstrip('\n') + ':' + obj.name.rstrip('\n') + ':' + obj.version.rstrip('\n').replace(":", "D") +'"')
    result.write('\n      purl: "pkg:deb/debian/' + obj.name.rstrip('\n') + '@' + obj.version.rstrip('\n').replace(":", "D") + '?distro=bullseye"')
    if len(obj.copyright) > 0:
        result.write('\n      authors:')
        for c in range(len(obj.copyright)):
            text = obj.copyright[c]
            result.write('\n      - "' + text.replace('"', "'") + '"')
    result.write('\n      declared_licenses:')
    if len(obj.license) == 0:
        result.write('\n      - "NONE"')
        result.write('\n      declared_licenses_processed:')
        result.write('\n        spdx_expression: "NONE"')
    else:
        for l in range(len(obj.license)):
            result.write('\n      - "' + obj.license[l] + '"')
        d_license_p = separator.join(obj.license)
        result.write('\n      declared_licenses_processed:')
        result.write('\n        spdx_expression: "' + d_license_p + '"')
    text = ''
    for line in obj.description:
        if line == obj.description[0]:
            if len(obj.description) == 1:
                text = line.rstrip('\n').replace('"', "'") + '"'
            else:
                text = text + line.rstrip('\n').replace('"', "'") + '\\\n'
        elif line == obj.description[-1]:
            text = text + '        \ ' + line.rstrip('\n').replace('"', "'") + '"'
        else:
            text = text + '        \ ' + line.rstrip('\n').replace('"', "'") + '\\\n'
    result.write('\n      description: "' + text)
    result.write('\n      homepage_url: "' + obj.homepage.rstrip('\n') + '"')
    result.write('\n      binary_artifact:')
    result.write('\n        url: "' + repository + obj.filename.rstrip('\n') + '"')
    result.write('\n        hash:')
    result.write('\n          value: "' + obj.sha256.rstrip('\n') + '"')
    result.write('\n          algorithm: "SHA-256"')
    result.write('\n' + tail)

result.write('\n    dependency_graphs:')
result.write('\n      Debian:')
result.write('\n        packages:')
for obj in list_installed_pkgs:
    result.write('\n        -  "Debian:' + obj.architecture.rstrip('\n') + ':' + obj.name.rstrip('\n') + ':' + obj.version.rstrip('\n').replace(":", "D") +'"')
result.write('\n        scopes:')
result.write('\n          :' + project + ':default:')
result.write('\n          - root: 0')
i = 0
for obj in list_installed_pkgs:
    i = i + 1
    if i > 1:
        result.write('\n          - root: ' + str(i-1))
result.write('\n        nodes:')
result.write('\n        - {}')
i = 0
for obj in list_installed_pkgs:
    i = i + 1
    if i > 1:
        result.write('\n        - pkg: ' + str(i-1))
result.write('\n        edges: []')
result.write('\n    has_issues: false')
result.write('\nscanner: null')
result.write('\nadvisor: null')
result.write('\nevaluator: null')

status.close()
compliance_data.close()
license_mapping.close()
for f in packages:
    f.close()
result.close()

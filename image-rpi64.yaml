{{ $architecture := or .architecture "arm64" }}
{{ $type := or .type "fixedfunction" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "v2025dev2" }}
{{ $osname := or .osname "apertis" }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) }}
{{ $image := or .image (printf "apertis_%s-%s-%s-rpi64" $suite $type $architecture) }}

{{ $cmdline := or .cmdline " rootwait ro quiet splash plymouth.ignore-serial-consoles fsck.mode=auto fsck.repair=yes cma=128M" }}

{{ $demopack := or .demopack "disabled" }}
{{ if eq $type "fixedfunction" }}
{{ $demopack := "disabled" }}
{{ end }}

{{- $local_repo_path := or .local_repo_path "" -}}
{{- $extra_packages := or .extra_packages "" -}}

{{- $unpack := or .unpack "true" }}

architecture: {{ $architecture }}

actions:
{{- if eq $unpack "true" }}
  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}.tar.gz
{{- end }}

  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "fixedfunction" }}
    imagesize: 4G
{{ else }}
    imagesize: 15G
{{end}}
    partitiontype: msdos
    mountpoints:
      - mountpoint: /
        partition: system
      - mountpoint: /boot
        partition: boot
        options: [ x-systemd.automount ]
      - mountpoint: /boot/firmware
        partition: firmware
        options: [ x-systemd.automount ]
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: firmware
        fs: vfat
        start: 0%
        end: 64M
      - name: boot
        fs: ext2
        start: 64M
        end: 320M
        flags: [ boot ]
      - name: system
        fs: ext4
        start: 320M
{{ if eq $type "fixedfunction" }}
        end: 3064M
{{ else }}
        end: 6064M
{{ end }}
      - name: general_storage
        fs: ext4
{{ if eq $type "fixedfunction" }}
        start: 3064M
{{ else }}
        start: 6064M
{{ end }}
        end: 100%

  - action: filesystem-deploy
    setup-kernel-cmdline: true
    append-kernel-cmdline: {{ $cmdline }}
    description: Deploying ospack onto image

  - action: recipe
    description: Test on Merge Request
    recipe: snippet-merge-request-test.yaml
    variables:
      local_repo_path: {{ $local_repo_path }}
      suite: {{ $suite }}
      extra_packages: {{ $extra_packages }}

  - action: overlay
    description: Set the default bootcounter
    source: overlays/default-uboot-bootcount

  - action: overlay
    description: "Enable USB automount"
    source: overlays/usb-automount-rules

  # on arm64 the initramfs post-install does not call zz-u-boot-menu from
  # u-boot-menupackage when it is installed at the same time as the kernel
  # work around it by installing the the boot configuration tools first
  # see https://phabricator.apertis.org/T6325
  - action: apt
    description: Boot configuration packages
    packages:
      - initramfs-tools
      - u-boot-menu

  - action: apt
    description: Kernel and system packages
    packages:
      - linux-image-{{$architecture}}
      - e2fsprogs

  - action: apt
    description: U-Boot package
    packages:
      - u-boot-rpi

  - action: apt
    description: Firmware packages
    packages:
      - raspi-firmware
      - firmware-brcm80211

  - action: apt
    description: Autoconfiguration packages for RPi64
    packages:
      - rpi64-autoconfig-connman

  - action: overlay
    description: "Default connman settings"
    source: overlays/connman

  - action: recipe
    description: Test on Merge Request cleanup
    recipe: snippet-merge-request-test-cleanup.yaml
    variables:
      local_repo_path: {{ $local_repo_path }}
      suite: {{ $suite }}

  - action: run
    description: Switch to live APT repos
    chroot: true
    script: scripts/switch-apt-to-live.sh -r {{ $suite }}

  - action: run
    description: Install Raspberry Pi boot firmware
    chroot: true
    command: sh -c "cp -av /usr/lib/raspi-firmware/* /boot/firmware/"

  - action: run
    description: Install U-Boot
    chroot: true
    command:  sh -c "cp -av /usr/lib/u-boot/rpi_arm64/u-boot.bin /boot/firmware/"

  - action: run
    description: Copy DTBs from u-boot
    chroot: true
    command:  sh -c "cp -av /usr/lib/u-boot/rpi_arm64/*.dtb /boot/firmware/"

  - action: run
    description: Create DTB Overlays directory
    chroot: true
    command:  sh -c "mkdir -p /boot/firmware/overlays"

  - action: run
    description: Copy DTB Overlays from u-boot
    chroot: true
    command:  sh -c "cp -av /usr/lib/u-boot/rpi_arm64/bcm2711-vl805.dtbo /boot/firmware/overlays/vl805.dtbo"

  - action: overlay
    description: Copy config.txt
    source: overlays/raspberrypi/firmware
    destination: /boot/firmware

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
  {{ if ne $demopack "disabled" }}
  # Use wget to get some insight about https://phabricator.collabora.com/T11930
  # TODO: Revert to a download action once the cause is found
  - action: run
    description: Download multimedia demo pack
    chroot: false
    command: wget --debug {{ $demopack }} -O "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  - action: unpack
    description: Unpack multimedia demo pack
    compression: gz
    file: multimedia-demo.tar.gz

  - action: run
    description: Clean up multimedia demo pack tarball
    chroot: false
    command: rm "${ARTIFACTDIR}/multimedia-demo.tar.gz"
  {{ end }}


  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $image }}.img.pkglist.gz"

  - action: run
    description: "Save installed package changelogs"
    chroot: false
    script: scripts/save_changelogs.py
      --root "$ROOTDIR"
      --out "${ARTIFACTDIR}/{{ $image }}.changelogs.tar.gz"

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.sh

  # the clearing of machine-id can't be done before this point since
  # systemd-boot requires the machine-id to be set for reasons related to
  # dual-boot scenarios:
  # * to avoid conflicts when creating entries, see the `90-loaderentry` kernel
  #   install trigger
  # * to set the entries for the currently booted installation as default in
  #   the loader.conf generated by `bootctl install`
  #
  # in our image this is not useful, as the actual machine-id is supposed to be
  # uniquely generated on the first boot. however the impact is negligible, as
  # things still work albeit the code used to potentially disambiguate entries
  # doesn't match a real machine-id
  - action: run
    chroot: false
    description: "Empty /etc/machine-id so it's regenerated on first boot with an unique value"
    command: truncate -s0 "${ROOTDIR}/etc/machine-id"

  - action: run
    description: Generate licensing BOM file
    chroot: false
    script: >-
            scripts/generate_licensing_bom.py --copyright --dir "${ROOTDIR}/usr/share/doc" 
            --dpkg-status "${ROOTDIR}/var/lib/dpkg/status" --verbose 2 > ${ARTIFACTDIR}/{{ $image }}.img.licenses

  - action: run
    description: Download apt sources
    chroot: false
    script: >-
            scripts/get_apt_lists.sh -a {{ $architecture }}
            -k "${ROOTDIR}/etc/apt/trusted.gpg.d/{{ $osname }}-archive-keyring.gpg"
            -m {{ $mirror }} -o ${ARTIFACTDIR}/apt_lists -s {{ $suite }}

  - action: run
    description: Generate security BOM file
    chroot: false
    script: >-
            scripts/generate_security_bom.py --dir "${ROOTDIR}/usr/share/doc" --dpkg-status "${ROOTDIR}/var/lib/dpkg/status"
            --arch {{ $architecture }} --branch {{ $osname }}/{{ $suite }} --apt-list-dir ${ARTIFACTDIR}/apt_lists
            --out ${ARTIFACTDIR}/{{ $image }}.img.buildeps

  - action: run
    description: Delete /usr/share/doc
    chroot: false
    command: rm -rf "${ROOTDIR}"/usr/share/doc/*

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.img.filelist.gz"

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create "${ARTIFACTDIR}/{{ $image }}.img" > "${ARTIFACTDIR}/{{ $image }}.img.bmap"

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f "${ARTIFACTDIR}/{{ $image }}.img"

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum "${ARTIFACTDIR}/{{ $image }}.img.gz" > "${ARTIFACTDIR}/{{ $image }}.img.gz.sha256"
